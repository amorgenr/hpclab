# High-Performance Computing Lab for CSE 

## News

* Lecture begins on Monday, February 17., 13:15 (11.02.20)

## General information

**Lecturers**   : [Olaf Schenk](mailto:oschenk@ethz.ch),
                  [Roger K&auml;ppeli](mailto:roger.kaeppeli@sam.math.ethz.ch)

**Assistants**  : [Aryan Eftekhari](mailto:aryan.eftekhari@usi.ch),
                  [Dimosthenis Pasadakis](mailto:dimosthenis.pasadakis@usi.ch),
                  [Kardos Juraj](mailto:juraj.kardos@usi.ch),
                  [Radim Janal&iacute;k](mailto:radim.janalik@usi.ch),
                  [Pratyuksh Bansal](mailto:pratyuksh.bansal@sam.math.ethz.ch),
                  [Charilaos Mylonas](mailto:mylonas@ibk.baug.ethz.ch),
                  and [Georg Pollak](mailto:pollakg@student.ethz.ch)

**Mailing list**: <hpclab_fs20@sympa.ethz.ch>

**When** : Monday, 13:15 - 17:00

**Where**: [HG](http://www.mapsearch.ethz.ch/map/mapSearchPre.do?gebaeudeMap=HG&geschossMap=E&raumMap=41&farbcode=c010&lang=en) [E 41](http://www.rauminfo.ethz.ch/Rauminfo/grundrissplan.gif?gebaeude=HG&geschoss=E&raumNr=41&lang=en) [>>](http://www.rauminfo.ethz.ch/Rauminfo/RauminfoPre.do?gebaeude=HG&geschoss=E&raumNr=41&lang=en)

For questions or remarks, please use the mailing list:
<hpclab_fs20@sympa.ethz.ch>


## Course Description
This HPC Lab for CSE will focus on the effective exploitation of
state-of-the-art HPC systems with a special focus on Computational Science
and Engineering.
The content of the course is tailored for 3rd year Bachelor students
interested in both learning parallel programming models, scientific
mathematical libraries, and having hands-on experience using HPC systems.


## Contents
A goal of the course is that students will learn the principles and practices
of basic numerical methods and HPC to enable large-scale scientific
simulations.
This goal will be achieved within six to eight mini-projects with a focus on
HPC and CSE.
The students will learn the principles and practices of basic parallel
numerical computation.
These projects might be accompanied by reading assignments on the theory of
the selected numerical method or the programming language.
The course consists of hand-on projects on HPC and CSE.
Students will also be offered a number of exercises on performance analysis,
parallelism detection and efficient development for modern manycore
processors using OpenMP and MPI.
This experience will be very useful for individual semester projects in other
classes, where students might develop & deploy components of massively
parallel numerical simulation software.

GPU computing will not be covered in this course.
It will be fully discussed in the annual CSCS-USI summer school on Effective
High-Performance Computing & Data Analytics with GPUs where we will focus on
the effective exploitation of state-of-the-art hybrid High-Performance
Computing (HPC) systems with a special focus on Data Analytics.
The content of the summer school course is tailored for intermediate
graduate students interested in both learning parallel programming models,
and having hands-on experience using HPC systems.
Starting from an introductory explanation of the available systems at CSCS,
the course will progress to more applied topics such as parallel programming
on accelerators, scientific libraries, and deep learning software frameworks.
The following topics will be covered: GPU architectures, GPU programming,
Message passing programming model (MPI), Performance optimization and
scientific libraries, interactive supercomputing, Python libraries,
Introduction to Machine Learning, and GPU optimized framework.
The Summer School will be held from July 13 to 24, 2020, at the Steger
Center in Riva San Vitale, located in the Italian area of Switzerland.
More information is available at this [Link](https://www.cscs.ch/events/upcoming-events/event-detail/cscs-usi-summer-school-2020/). 
Students will be able to earn six ECT credit points for this
[course from USI](https://search.usi.ch/en/courses/35260874/effective-high-performance-computing-data-analytics-summer-schoo)
(subject to exam).


## Lecture slides, projects and solutions
Lecture slides, projects and solutions will be provided as part of this
git repository.


## Lab & Coursework
The lab requirements include programming of mini-projects and other
assignments; final oral examination.
All of these will count in your final grade.
The final grade will be calculated by averaging the two elements with weights

  * up to six to eight mini-projects: 40%.
  * final oral exam: 60%
  * Value in ECTS: 7


## Programming
We will use C/C++, Matlab, MPI, and the Intel Math Kernel Library for the
mini-project programs.


## Final oral exam
In the oral exam, we will review these mini-projects and the theory behind
it, and discuss various aspects of these methods (performance, numerical
algorithms).
The oral exam will cover material from the entire course and we might discuss
your solutions.


## Regrading requests
If you feel that you deserve a better grade for a mini-project, you may
submit a regrade request.
It must be in writing to the TA.
This regrading request must be done within one week after the assignment has
been graded by the TA.
Your request should briefly summarize why you feel that the original grade
was unfair.
Your TA will take a day or two to reevaluate your assignment, and then issue
a decision.
If you are still not satisfied, you can appeal the decision to Professor
Schenk and Dr. Käppeli (Again, the appeal must be in writing.)
Note that your entire assignment may be reevaluated, not just the question
that you submit for regrading.


## Late day policy
All assignments are due in class on the assigned due date.
We recognize that students may face unusual circumstances and require some
flexibility in the course of the semester, therefore each student will be
granted ONE free assignment that we will not count for the final grading of
the mini-projects.


## Assignment collaboration policy
You are allowed to discuss such questions with anyone you like; however:

* Your submission must list anyone you discussed problems with.

* You must write up your submission independently.


## Submission
If you want to receive feedback on your exercises, please submit/upload your
solution to this moodle webpage:

  <https://moodle-app2.let.ethz.ch/course/view.php?id=12315>

Your exercise will then be corrected within a period of three weeks (maybe
earlier).

## Exam information 

* TBA (if possible August 03-06, subject to ETH approval)

## Useful links on numerical methods

 * C. Greif and U. Ascher: [A First Course in Numerical Methods](https://www.amazon.com/Numerical-Methods-Computational-Science-Engineering/dp/0898719976) is designed for students and researchers who seek practical knowledge of modern techniques in scientific computing. Avoiding encyclopedic and heavily theoretical exposition, the book provides an in-depth treatment of fundamental issues and methods, the reasons behind the success and failure of numerical software, and fresh and easy-to-follow approaches and techniques. The book takes an algorithmic approach, focusing on techniques that have a high level of applicability to engineering, computer science, and industrial mathematics.
  ([ETH link](https://doi.org/10.1137/9780898719987))

## Useful links on computing

* Hager and G. Wellein: [Introduction to High Performance Computing for Scientists and Engineers](http://www.amazon.de/Introduction-Performance-Computing-Scientists-Computational/dp/143981192X), ISBN 143981192X, CRC Computational Science Series, 2010.
  ([ETH link](https://doi.org/10.1201/EBK1439811924))

* S. Goedecker and A. Hoisie: [Performance Optimization of Numerically Intensive Codes](http://books.google.de/books?hl=de&id=dQpi46dLJ8gC&dq=Goedecker+Hoisie+optimization&printsec=frontcover&source=web&ots=Clo353OjPc&sig=Mi3qYwEgOdXvao29Caj3RWNcGwg). Like many HPC books, slightly outdated but still very useful. Does not cover recent developments like EPIC and SIMD instruction sets.
  ([ETH link](https://doi.org/10.1137/1.9780898718218))

* K. Wadleigh and I. Crawford: Software Optimization for High Performance Computing. This is a very good book that is however slightly outdated and unfortunately out of print. You can get it at,e.g., [abebooks.com](https://www.abebooks.com/servlet/BookDetailsPL?bi=22854376050&searchurl=an%3DWadleigh%2Band%2BCrawford%26sortby%3D17&cm_sp=snippet-_-srp1-_-title1).

* K. Dowd and C. Severance: High Performance Computing (RISC Architectures, Optimization & Benchmarks). Covers traditional optimization techniques in detail. Out of print but available at [Amazon.com](https://www.abebooks.com/servlet/BookDetailsPL?bi=30124276077&searchurl=kn%3DHigh%2BPerformance%2BComputing%2B%2528RISC%2BArchitectures%252C%2BOptimization%2B%2526%2BBenchmarks%2529.%26sortby%3D17&cm_sp=snippet-_-srp1-_-title1) etc.

* R. Gerber: [The Software Optimization Cookbook](https://www.amazon.com/Software-Optimization-Cookbook-Performance-Platforms/dp/0976483211). Contains also some material on SIMD programming with SSE.

* H. Katzgraber: [Scientific Software Engineering in a Nutshell](http://arxiv.org/abs/0905.1628).

* The SGI Origin 2000 and Onyx2 [Performance Tuning and Optimization Guide](https://manx-docs.org/details.php/109,19541) is still a great resource for learning the basics of code optimization, although those machines have long since disappeared. Just ignore the system-specific stuff.
   
* A. Grama, A. Gupta, G. Karypis, V. Kumar: Introduction to parallel computing. In-depth treatment of parallel hardware models, parallel programming paradigms and parallel algorithms. Available at [Link](https://www-users.cs.umn.edu/~karypis/parbook/).

* T. Mattson, B. Sanders, B. Massingill: Patterns for Parallel Programming. A compendium of concepts required for parallel program design, accompanied by sample implementations. Available at [Amazon.com](https://www.amazon.com/Patterns-Parallel-Programming-paperback-Software/dp/0321940784).
    
* W. Gropp, E. Lusk, A. Skjellum: [Using MPI - Portable Parallel Programming with the Message-Passing Interface](http://www.amazon.com/Using-MPI-Programming-Engineering-Computation/dp/0262571323), MIT Press, 1999. It covers the basics without too much fluff.
    
* MPI documentation can be found online in the [MPI forum](http://www.mpi-forum.org/).
    
* The [official OpenMP](http://www.openmp.org/blog/specifications/) specification contains quite a lot of good examples and can therefore be used as a good and readable reference for more information on OpenMP.
   
* R. Chapman, G. Jost and R. van der Pas: [Using OpenMP](https://www.amazon.com/Using-OpenMP-Programming-Engineering-Computation/dp/0262533022). This book not only gives a thorough introduction to OpenMP but also covers the most relevant performance and correctness issues, together with best practices.
 
* Introductory, very basic tutorials from Lawrence Livermore National Laboratory (LLNL): [Introduction to Parallel Computing](https://computing.llnl.gov/tutorials/parallel_comp/), [OpenMP tutorial](https://computing.llnl.gov/tutorials/openMP/), [MPI Tutorial](https://computing.llnl.gov/tutorials/mpi/)

(follow the ETH links above if you are within the ETH network
(student-PCs/ETH WLAN/VPN) to get access to the Online-Versions.)

