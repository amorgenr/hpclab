/*
    Please include compiler name below (you may also include any other modules you would like to be loaded)

COMPILER= gnu

    Please include All compiler flags and libraries as you want them run. You can simply copy this over from the Makefile's first few lines

CC = cc
OPT = -O3
CFLAGS = -Wall -std=gnu99 $(OPT)
MKLROOT = /opt/intel/composer_xe_2013.1.117/mkl
LDLIBS = -lrt -Wl,--start-group $(MKLROOT)/lib/intel64/libmkl_intel_lp64.a $(MKLROOT)/lib/intel64/libmkl_sequential.a $(MKLROOT)/lib/intel64/libmkl_core.a -Wl,--end-group -lpthread -lm

*/

#include <immintrin.h>

const char* dgemm_desc = "Naive, three-loop dgemm.";

/* This routine performs a dgemm operation
 *  C := C + A * B
 * where A, B, and C are lda-by-lda matrices stored in column-major format.
 * On exit, A and B maintain their input values. */

void square_dgemm (int n, double* A, double* B, double* C)
{
  // TODO: Implement the blocking optimization
  int block_size = 8;
  double temp = 0;
  for (int bj = 0; bj < n; bj += block_size){
    for (int bk = 0; bk < n; bk += block_size){
    // Temp Variable to store result of block mult
      for (int i = 0; i < n; i++){
        for (int j = bj; j < min(bj + block_size, n); j++){
          temp = 0;
          for (int k = bk; k < min(bk + block_size, n); k++){
            temp += A[i + n*k] * B[k + n*j];
          }
          C[i + n*j] += temp;
        }
      }
    }
  }
}
/*
void square_dgemm (int n, double* A, double* B, double* C)
{
  // TODO: Implement the blocking optimization
  int block_size = 8;
  double temp = 0;
  for (int bi = 0 ; bi < n; bi += block_size){
    for (int bj = 0; bj < n; bj += block_size){
      for (int bk = 0; bk < n; bk += block_size){
        __m512d rightRow[8];
        __m512d resultRow[8];

        for (int i = 0; i < 8; ++i)
        {
            rightRow[i] = _mm512_loadu_pd((const float *)B[n*bj + bk]);
            resultRow[i] = _mm512_loadu_pd((const float *)C[n*bj + bi]);
        }
        for (int i = 0; i < 8; ++i)
        {
            for (int j = 0; j < 8; ++j)
            {
                resultRow[i] = _mm512_add_pd(resultRow[i], _mm512_mul_pd(rightRow[j], _mm512_set1_pd(A[bi + n*bk])));
            }
            _mm512_storeu_pd((double *)C[bi + n*bj], resultRow[i]);
        }
      }

    }
  }
}
*/
int min(int a, int b){
  return (((a)<(b))?(a):(b));
}
